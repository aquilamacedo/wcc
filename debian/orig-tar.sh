#!/bin/sh -e

set -x
# called by uscan with '--upstream-version' <version> <file>
PKG=wcc
UPSTREAMURL=https://github.com/endrazine/wcc.git
DIR=$PKG-$2
TAR=../${PKG}_$2+dfsg.orig.tar.gz

# get back the official wcc git repo, seems to be needed to regenerate the
# submodules content
# its git directory will be used to call git submodule update --init (see
# below)
tmpdir=`mktemp -d`
git clone ${UPSTREAMURL} ${tmpdir}/wcc

tar xvf $3
cd $DIR && cp -r ${tmpdir}/wcc/.git .git && git submodule update --init && rm -rf .git && cd $OLDPWD

# clean up the upstream tarball, yet not needed as copyright file seems to do
# the job also (orig-tar.exclude to delete ?)
tar -c -z -f $TAR $DIR
rm -rf $DIR $3
rm -rf ${tmpdir}

exit 0

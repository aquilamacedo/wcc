wcc (0.0.2+dfsg-4.2) UNRELEASED; urgency=low

  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 26 Jul 2020 04:24:33 -0000

wcc (0.0.2+dfsg-4.1) unstable; urgency=medium

  * NMU: Fix build (Closes: #950910)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 21 Mar 2020 18:02:12 +0100

wcc (0.0.2+dfsg-4) unstable; urgency=medium

  * Updated binutils usage, making libbfd linked staticaly (Closes: #949601)
  * Removed wsh test on PIE exe (ssh) as PIE exe are not supported.
    This is a wcc upstream constraint (Closes: #948613)
  * Add Reproductible build patch, removing compile time info in binary.
  * Corrected chunk position in wcc.c patch.

 -- Philippe Thierry <philou@debian.org>  Thu, 06 Feb 2020 09:11:20 +0100

wcc (0.0.2+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org
  * Fix broken dependency on mktemp in DEP-8 test.
    Thanks to Matthias Klose for the patch. (Closes: #890043)
  * Update Standards-Version to 4.1.3 (no change required)
  * Add lua to Suggests to appease lintian for /usr/share/wcc/scripts/print_G

 -- Raphaël Hertzog <hertzog@debian.org>  Sat, 10 Mar 2018 00:46:15 +0100

wcc (0.0.2+dfsg-2) unstable; urgency=medium

  * Various update (Closes: #873783): see belowing comments:
  * using packaged openlibm instead of embedded one.
    embedded openlibm was not able to be built on various arches
  * reducing target to amd64.
    the ldscript used to map relocated section in wsh has only been made for
    this target, making impossible to build wsh for other target than amd64
    without building specific ldscripts for each of them
  * add support to Debian/kfreebsd and Debian/Hurd.
    by embedding the elf-em macros list only when needed
  * This update also correct the binutils dependency (Closes: 873821).
    The error comes from an autogenerated dependency version build
    to binutils
    the udate as the same effect as a binNMU request, the package being
    rebuilt
  * Update std-version to 4.1.0

 -- Philippe Thierry <phil@reseau-libre.net>  Thu, 31 Aug 2017 22:31:12 +0200

wcc (0.0.2+dfsg-1) unstable; urgency=low

  [ Philippe Thierry ]
  * Initial release (Closes: #861390).
  * Updated various spelling error.
  * Updated man pages.
  * Add upstream changelog generation from git commit msg.
  * Updated upstream README to clean installation specific content.

  [ Raphaël Hertzog ]
  * Fix the permissions of the various script files.

 -- Philippe Thierry <phil@reseau-libre.net>  Tue, 23 May 2017 10:32:24 +0200

## The following commands are built into wsh

### help

Simply typing help in the wsh interpreter displays the following help


	> help
	  [Shell commands]

		help, quit, exit, shell, exec, clear

	  [Functions]

	 + basic:
		help(), man()

	 + memory display:
		 hexdump(), hex_dump(), hex()

	 + memory maps:
		shdrs(), phdrs(), map(), procmap(), bfmap()

	 + symbols:
		symbols(), functions(), objects(), info(), search(), headers()

	 + memory search:
		grep(), grepptr()

	 + load libaries:
		loadbin(), libs(), entrypoints(), rescan()

	 + code execution:
		libcall()

	 + buffer manipulation:
		xalloc(), ralloc(), xfree(), balloc(), bset(), bget(), rdstr(), rdnum()

	 + control flow:
		 breakpoint(), bp()

	 + system settings:
		enableaslr(), disableaslr()

	 + settings:
		 verbose(), hollywood()

	 + advanced:
		ltrace()

	Try help("cmdname") for detailed usage on command cmdname.

	> 


The advanced help for help follow:

	> help("help")

		WSH HELP FOR FUNCTION help


	NAME

		help

	SYNOPSIS

		help([topic])

	DESCRIPTION

		Display help on [topic]. If [topic] is ommitted, display general help.

	RETURN VALUES

		None


	> 


### quit

The quit command terminates the main wsh process and exits the wsh interpreter.

Here is the help page for quit


	> help("quit")

		WSH HELP FOR COMMAND quit


	NAME

		quit

	SYNOPSIS

		quit 

	DESCRIPTION

		Exit wsh.

	RETURN VALUES

		Does not return : exit wsh



	> 


### exit

The exit command behaves much like the quit command.

Here is the detailed help for the exit command:


	> help("exit")

		WSH HELP FOR COMMAND exit


	NAME

		exit

	SYNOPSIS

		exit 

	DESCRIPTION

		Exit wsh.

	RETURN VALUES

		Does not return : exit wsh



	> 


#### Note on the exit command versus exit() function

It is worth noticing that typing exit(0) in the terminal does something different entirely : this will result in calling the function exit(), typically from the C library, with the parameter 0.


### shell

The shell command instanciates an instance of /bin/sh from the wsh interpreter. Terminating the /bin/sh session will allow returning in the parent wsh session.

	> help("shell")

		WSH HELP FOR COMMAND shell


	NAME

		shell

	SYNOPSIS

		shell [command]

	DESCRIPTION

		Run a /bin/sh shell.

	RETURN VALUES

		None. Returns uppon shell termination.


	> 

#### example usage of the shell command

From the wsh interpreter, the following commands start a /bin/sh shell, run the /bin/id application from this shell, and finally calls exit, which terminates the /bin/sh session and returns into the wsh interpreter.


	> shell
	$ id
	uid=1001(jonathan) gid=1001(jonathan) groups=1001(jonathan)
	$ exit
	> 



### exec

The exec command allows running an external command from the wsh interpreter.

Here is the detailed help page for the exec command :

	> help("exec")

		WSH HELP FOR COMMAND exec


	NAME

		exec

	SYNOPSIS

		exec <command>

	DESCRIPTION

		Run <command> via the system() library call.

	RETURN VALUES

		None. Returns uppon <command> termination.


	> 

#### Example usage of the exec command

The following command exemplifies calling the uname system utility with the "-a" argument:

	> exec uname -a
	Linux blackbox 3.13.0-68-generic #111-Ubuntu SMP Fri Nov 6 18:17:06 UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
	> 



### clear

The clear command clears the terminal. Its detailed help follows:

	> help("clear")

		WSH HELP FOR COMMAND clear


	NAME

		clear

	SYNOPSIS

		clear 

	DESCRIPTION

		Clear terminal.

	RETURN VALUES

		None.


	> 



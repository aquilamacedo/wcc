

This page documents how to download, compile and install WCC.

## Downloading the source code
The official codebase of the Witchcraft Compiler Collection is hosted on github at https://github.com/endrazine/wcc/ . It uses git modules, so some extra steps are needed to fetch all the code including depedencies. To download the source code of wcc, in a terminal, type:

    git clone https://github.com/endrazine/wcc.git
    cd wcc
    git submodule init
    git submodule update

This will create a directory named wcc and fetch all required source code in it.

## Prerequisites

### Installing requirements
The Witchcraft Compiler Collection requires the following software to be installed:

        Glibc, libbfd, libdl, zlib, libelf, libreadline, libgsl.

### Installing requirements on Ubuntu/Debian
Under ubuntu/debian those dependancies can be installed with the following command:

    sudo apt-get install clang libbfd-dev uthash-dev libelf-dev libcapstone-dev libreadline6 libreadline6-dev libiberty-dev libgsl-dev

## Building and Installing:
#### Building WCC
From your root wcc directory, type:

    make
#### Installing WCC
Then to install wcc, type:

    sudo make install

#### Building the WCC documentation
WCC makes use of doxygen to generate its documentation. From the root wcc directory, type

    make documentation

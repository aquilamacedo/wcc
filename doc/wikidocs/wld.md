### wld : The Witchcraft Linker.
wld takes an ELF executable as an input and modifies it to create a shared library.
#### wld command line options
	jonathan@blackbox:~$ wld
	Witchcraft Compiler Collection (WCC) version:0.0.1    (23:11:13 Jul 21 2016)

	Usage: wld [options] file

	options:

	    -libify          Set Class to ET_DYN in input ELF file.

	jonathan@blackbox:~$ 
#### Example usage of wld
The following example libifies the executable /bin/ls into a shared library named /tmp/ls.so.

	jonathan@blackbox:~$ cp /bin/ls /tmp/ls.so
	jonathan@blackbox:~$ wld -libify /tmp/ls.so
	jonathan@blackbox:~$ 

#### Limits of wld
wld currently only works on ELF binaries. However wld can process ELF executables irrelevant of their architecture or operating system. wld could for instance process Intel, ARM or SPARC executables from Android, Linux, BSD or UNIX operating systems and transform them into "non relocatable shared libraries". Feel free to refer to the documentation under the /doc directory for more ample details.


#### Do I even need wld ?

If the ELF executable you whish to work with has been compiled with as Position Independant Executable (-pie -fpie compiler flags with gcc or clang), it already is a functional shared library and doesn't need to be libified. In particular, its ELF header is already set to ET_DYN.

Here is an example executable that is of type ET_EXEC and can be libified. Mind the Type field set to EXEC:

	jonathan@blackbox:~$ file /bin/ls
	/bin/ls: ELF 64-bit LSB  executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.24, BuildID[sha1]=8d0966ce81ec6609bbf4aa439c77138e2f48a471, stripped
	jonathan@blackbox:~$ readelf -h /bin/ls
	ELF Header:
	  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
	  Class:                             ELF64
	  Data:                              2's complement, little endian
	  Version:                           1 (current)
	  OS/ABI:                            UNIX - System V
	  ABI Version:                       0
	  Type:                              EXEC (Executable file)
	  Machine:                           Advanced Micro Devices X86-64
	  Version:                           0x1
	  Entry point address:               0x404890
	  Start of program headers:          64 (bytes into file)
	  Start of section headers:          108288 (bytes into file)
	  Flags:                             0x0
	  Size of this header:               64 (bytes)
	  Size of program headers:           56 (bytes)
	  Number of program headers:         9
	  Size of section headers:           64 (bytes)
	  Number of section headers:         28
	  Section header string table index: 27
	jonathan@blackbox:~$ 


Here is an exemple binary compiled as Position Independant Executable and not requiring libification to be used as a shared library or loaded in wsh. Mind the Type field set to DYN:

	jonathan@blackbox:~$ file /usr/sbin/apache2
	/usr/sbin/apache2: ELF 64-bit LSB  shared object, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.24, BuildID[sha1]=02c74092325980f41ca3e1c2995daec1f3b30ea2, stripped
	jonathan@blackbox:~$ readelf -h /usr/sbin/apache2
	ELF Header:
	  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
	  Class:                             ELF64
	  Data:                              2's complement, little endian
	  Version:                           1 (current)
	  OS/ABI:                            UNIX - System V
	  ABI Version:                       0
	  Type:                              DYN (Shared object file)
	  Machine:                           Advanced Micro Devices X86-64
	  Version:                           0x1
	  Entry point address:               0x37156
	  Start of program headers:          64 (bytes into file)
	  Start of section headers:          635736 (bytes into file)
	  Flags:                             0x0
	  Size of this header:               64 (bytes)
	  Size of program headers:           56 (bytes)
	  Number of program headers:         9
	  Size of section headers:           64 (bytes)
	  Number of section headers:         28
	  Section header string table index: 27
	jonathan@blackbox:~$ 


Finally, here is what a libified shared library looks like. The Type field has been set to DYN by wld during the libification process:


	jonathan@blackbox:~$ file /tmp/ls.so 
	/tmp/ls.so: ELF 64-bit LSB  shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=04fd28208b659339be2711ea5f6d3485b6117da6, not stripped
	jonathan@blackbox:~$ readelf -h /tmp/ls.so 
	ELF Header:
	  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
	  Class:                             ELF64
	  Data:                              2's complement, little endian
	  Version:                           1 (current)
	  OS/ABI:                            UNIX - System V
	  ABI Version:                       0
	  Type:                              DYN (Shared object file)
	  Machine:                           Advanced Micro Devices X86-64
	  Version:                           0x1
	  Entry point address:               0x6200
	  Start of program headers:          64 (bytes into file)
	  Start of section headers:          2261504 (bytes into file)
	  Flags:                             0x0
	  Size of this header:               64 (bytes)
	  Size of program headers:           56 (bytes)
	  Number of program headers:         6
	  Size of section headers:           64 (bytes)
	  Number of section headers:         27
	  Section header string table index: 24
	jonathan@blackbox:~$ 


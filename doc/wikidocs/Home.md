### The Witchcraft Compiler Collection User Manual

Welcome to the Witchcraft Compiler Collection User Manual.

Feel free to edit and contribute to this Manual.

The latest version of this manual is available at:

https://github.com/endrazine/wcc/wiki

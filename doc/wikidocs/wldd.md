### wldd : print shared libraries compilation flags
When compiling C code, it is often required to pass extra arguments to the compiler to signify which shared libraries should explicitely linked against the compile code. Figuring out those compilation parameters can be cumbersome. The wldd commands displays the shared libraries compilation flags given at compile time for any given ELF binary.

#### wldd command line options

	jonathan@blackbox:~$ wldd 
	Usage: /usr/bin/wldd </path/to/bin>

	  Returns libraries to be passed to gcc to relink this application.

	jonathan@blackbox:~$ 

#### Example usage of wldd


##### On /bin/ls (ET_EXEC ELF executable)

The following command displays shared libraries compilation flags as passed to gcc when compiling /bin/ls from GNU binutils:

	jonathan@blackbox:~$ wldd /bin/ls
	-lselinux -lacl -lc -lpcre -ldl -lattr 
	jonathan@blackbox:~$

##### On apache2 (ET_DYN ELF executable compiled as Position Independant Executable / full ASLR)

The following command displays the compilation flags relative to shared libraries used when compiling /usr/sbin/apache2:

	jonathan@blackbox:~$ wldd /usr/sbin/apache2
	-lpcre -laprutil-1 -lapr-1 -lpthread -lc -lcrypt -lexpat -luuid -ldl 
	jonathan@blackbox:~$ 

##### On the openssl shared library

This command can also be ran on shared libraries. The following example displays the same compiler options for the openssl shared library:

	jonathan@blackbox:~$ wldd /usr/lib/x86_64-linux-gnu/libssl.so.0.9.8 
	-lcrypto -lc -ldl -lz 
	jonathan@blackbox:~$ 


### Security Caveat

wldd invokes binutils' ldd which in turns loads the binary passed as an argument using its hardcoded dynamic linker. This does run code inside the analysed binary. As such, running wldd on potentially hostile code (eg: malware) is not safe.


Note: We could get the name of the shared libraries linked with this binary from the content of its .dynamic section without having to rely on ldd nor run the binary. That would be very useful. It would also produce a non recursive answer (unlike wldd currenty), which would reflect more the actual linking of the binary. Feel free to implement it :)
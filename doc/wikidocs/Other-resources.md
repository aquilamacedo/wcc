## Presentations

The slides of the presentation given at the DEF CON 24 Conference in August 2016 are available at: https://github.com/endrazine/wcc/raw/master/doc/presentations/Jonathan_Brossard_Witchract_Compiler_Collection_Defcon24_2016.pdf

## More demos

The source code of the all demos of the presentation given at DEF CON can be found here : https://github.com/endrazine/wcc/tree/master/doc/presentations/demos_defcon24_2016

## Developper Manual

The Doxygen documentation of the Witchcraft Compiler Collection is available at: https://github.com/endrazine/wcc/raw/master/doc/WCC_internal_documentation.pdf



